#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "read-line.h"
#include "tty-raw-mode.c"

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

#define INIT_MAX_LINE 64
#define MAX_HISTORY 64

// when you use arrow up/down, these keep track of where you are in the history vector
int histIndex = 0;
int histIndexOrig = 0;

int cursorPos;                                                   // location of cursor on screen
int lineSize;                                                    // number of characters on current line
size_t maxLineSize;                                              // size of buffer for current line
char * lineBuf;                                                  // buffer for current line
char ** history = (char **)malloc(MAX_HISTORY * sizeof(char *)); // all past commands are stored here

void printableChar(char c) {
    int i;
    for (i = lineSize; i > cursorPos; i--) {
        lineBuf[i] = lineBuf[i - 1];
    }
    lineBuf[cursorPos] = c;
    for (i = cursorPos; i < lineSize + 1; i++) {
        write(STDOUT_FILENO, &lineBuf[i], 1);
    }
    c = 8;
    for (i = cursorPos; i < lineSize; i++) {
        write(STDOUT_FILENO, &c, 1);
    }
    lineSize++;
    cursorPos++;
    lineBuf[lineSize] = '\0';
}

void backspaceChar() {
    if (lineSize && cursorPos) {
        char c = 8;
        write(STDOUT_FILENO, &c, 1);

        int i;
        for (i = cursorPos; i < lineSize; i++) {
            lineBuf[i - 1] = lineBuf[i];
            write(STDOUT_FILENO, &lineBuf[i - 1], 1);
        }

        c = ' ';
        write(STDOUT_FILENO, &c, 1);

        c = 8;
        for (i = 0; i < lineSize - cursorPos + 1; i++)
            write(STDOUT_FILENO, &c, 1);

        cursorPos--;
        lineBuf[--lineSize] = '\0';
    }
}

void delToRight() {
    if (cursorPos != lineSize) {
        write(STDOUT_FILENO, &lineBuf[cursorPos++], 1);
        backspaceChar();
    }
}

// called when user uses arrow up and arrow down to browse shell history
void browseHistory(bool arrowUp) {
    char c;
    int i;

    // use MIN: don't backtrack past the beginning of the line
    for (i = 0; i < MIN(lineSize, cursorPos); i++) {
        c = 8;
        write(STDOUT_FILENO, &c, 1);
    }

    // then print spaces to cover old characters
    for (i = 0; i < lineSize; i++) {
        c = ' ';
        write(STDOUT_FILENO, &c, 1);
    }

    // print backspaces
    for (i = 0; i < lineSize; i++) {
        c = 8;
        write(STDOUT_FILENO, &c, 1);
    }

	// decide which way to go in the history
    if (arrowUp)
        lineBuf = strdup(history[--histIndex]);
    else
        lineBuf = strdup(history[++histIndex]);

    lineSize = strlen(lineBuf);
    cursorPos = lineSize;
    write(STDOUT_FILENO, lineBuf, lineSize);
}

void escapeSequence() {
    char c, c1, c2, c3;
    read(STDIN_FILENO, &c1, 1);
    read(STDIN_FILENO, &c2, 1);

    if (c1 == 91 && c2 == 65) { // up arrow: print next line in history
        if (histIndex)
            browseHistory(true);
    } else if (c1 == 91 && c2 == 66) { // down arrow: print previous line in history
        if (histIndex < histIndexOrig) 
            browseHistory(false);
    } else if (c1 == 91 && c2 == 68) { // left arrow: go left one character
        if (cursorPos > 0) {
            c = 8;
            write(STDOUT_FILENO, &c, 1);
            cursorPos--;
        }
    } else if (c1 == 91 && c2 == 67) { // right arrow: go right one character
        if (cursorPos < lineSize) {
            c = lineBuf[cursorPos++];
            write(STDOUT_FILENO, &c, 1);
        }
    } else if (c1 == 91 && c2 == 51 && read(STDIN_FILENO, &c3, 1) && c3 == 126) {
        // fn + delete on Macintosh: treat this the same as delete (ctrl + d)
        delToRight();
    }
}

void insertIntoHistory() {
    if (histIndexOrig == MAX_HISTORY) {
        free(history[0]);
        for (int i = 0; i < MAX_HISTORY - 2; i++) {
            history[i] = history[i + 1];
        }
        history[MAX_HISTORY - 1] = lineBuf;
    } else {
        history[histIndexOrig] = lineBuf;
    }
}

void initializeVars() {
    lineSize = 0;
    maxLineSize = INIT_MAX_LINE;
    cursorPos = 0;
    lineBuf = (char *)malloc(maxLineSize);
    lineBuf[0] = '\0';
    insertIntoHistory();
}

char * read_line() {
    tty_raw_mode();   // set terminal in raw mode
    initializeVars(); // initialize the global variables

    // read one line character by character until enter is typed
    char c;
    while (1) {
        if (lineSize + 2 == (int)maxLineSize) {
            maxLineSize *= 2;
            lineBuf = (char *)realloc((void *)lineBuf, maxLineSize);
        }

        read(STDIN_FILENO, &c, 1); // read one character
        if (c >= 32 && c != 127) { // printable character
            printableChar(c);
        } else if (c == 10) { // <enter>
            write(STDOUT_FILENO, &c, 1);
            break;
        } else if (c == 31) { // ctrl-?
            lineBuf[lineSize++] = '\0';
            break;
        } else if (c == 8 || c == 127) { // <backspace>
            backspaceChar();
        } else if (c == 27) { // escape sequence
            escapeSequence();
        } else if (c == 4) { // delete char to right of cursor
            delToRight();
        }
    }

    // don't clear current line until user hits <enter>
    // on a command from teh history
    if (histIndex != histIndexOrig) {
        free(history[histIndexOrig]);
        history[histIndexOrig] = lineBuf;
    }

    // don't let history go over MAX_HISTORY - 1
    // and don't insert blank lines into history
    if (histIndexOrig < MAX_HISTORY - 1 && lineBuf[0] != '\0') {
        histIndexOrig++;
        histIndex = histIndexOrig;
    }

    lineBuf[lineSize] = '\0'; // append null terminator
    char * ret = strdup(lineBuf);
    ret[lineSize] = '\n'; // append newline to returned string
    ret[lineSize + 1] = '\0';
    return ret;
}
