%{
	#include <string.h>
    #include <unistd.h>
    #include "y.tab.h"
    #include "read-line.h"
    int mygetc(FILE * f) {
        static char * p;
        char ch;
        if (!isatty(STDIN_FILENO)) {
            // stdin is not a terminal. Call real getc
            return getc(f);
        }
        // stdin is a terminal. Call our read_line
        if (p == NULL || *p == 0) {
            char * s = read_line();
            p = s;
        }
        ch = *p++;
        return ch;
    }
    #undef getc
    #define getc(f) mygetc(f)
%}
%%

[ \t]
\n 	     return NEWLINE;
">" 	 return GREAT;
"<"      return LESS;
"|"      return PIPE;
">&"     return GREATAMPERSAND;
">>"     return GREATGREAT;
">>&"    return GREATGREATAMPERSAND;
"&"      return AMPERSAND;

`[^\n`]*`           {
                        /* this expression should be run in a subshell */

                        /* remove backticks from expression */
                        yytext++;
                        yytext[strlen(yytext) - 1] = 0;
                        yylval.string = strdup(yytext);
                        
                        int fdpipe[2];
                        if (pipe(fdpipe) == -1) {
                            perror("subshell pipe");
                            exit(1);
                        }

                        /* handle output redirection */
                        int oldOut = dup(1);
                        dup2(fdpipe[1], 1);

                        /* create child process to fork a new shell */
                        int pid = fork();
                        if (pid == -1) {
                            perror("subshell fork 1");
                            exit(1);
                        }
                        if (pid == 0) {
                            char * myArgs[3];
                            myArgs[0] = getenv("_");
                            char * buf = malloc(strlen(yytext) + strlen("\nexit\n") + 1);
                            sprintf(buf, "%s\nexit\n", yytext);
                            myArgs[1] = buf;
                            myArgs[2] = NULL;

                            execvp(myArgs[0], myArgs);
                            perror("subshell fork 2");
                            exit(1);
                        }
                        close(fdpipe[1]);
                        dup2(oldOut, 1);

						/* read in characters forward from child shell and put them in main shell's buffer backward */
                        char c;
						size_t maxSize = 64;
                        char * buf = malloc(maxSize);
                        int index = 0;
                        while (read(fdpipe[0], &c, 1)) {
							if (index == maxSize) {
								maxSize *= 2;
								buf = realloc((void *)buf, maxSize);
							}
                            buf[index++] = (c == '\n' ? ' ' : c);
						}
                        close(fdpipe[0]);
                        
                        for (int i = index - 1; i >= 0; i--) {
                            yyunput(buf[i], yytext);
                        }
                    }

\"[^\n\"]*\"        {
                        /* remove quotes, then insert arg */
                        yytext++;
                        yytext[strlen(yytext) - 1] = 0;
                        yylval.string = strdup(yytext);
                        return WORD;
                    }

[^ \t\n][^ \t\n]*>[^ \t\n][^ \t\n]* {
                        char * yytextOrig = yytext;
                        while (*yytextOrig != '>') { yytextOrig++; }
                        *yytextOrig = 0;
                        yylval.string = strdup(yytext);
    
                        yytextOrig++;
                        while (*yytextOrig) { yytextOrig++; }
                        yytextOrig--;
                        while (*yytextOrig) {
                            yyunput(*yytextOrig, yytext);
                            yytextOrig--;
                        }
                        yyunput(' ', yytext);
                        yyunput('>', yytext);
    
                        return WORD;
                    }

[^ \t\n][^ \t\n]*<[^ \t\n][^ \t\n]* {
                        char * yytextOrig = yytext;
                        while (*yytextOrig != '<') { yytextOrig++; }
                        *yytextOrig = 0;
                        yylval.string = strdup(yytext);
    
                        yytextOrig++;
                        while (*yytextOrig) { yytextOrig++; }
                        yytextOrig--;
                        while (*yytextOrig) {
                            yyunput(*yytextOrig, yytext);
                            yytextOrig--;
                        }
                        yyunput(' ', yytext);
                        yyunput('<', yytext);
    
                        return WORD;
                    }

[^ \t\n][^ \t\n]*[|][^ \t\n][^ \t\n]* {
                        char * yytextOrig = yytext;
                        while (*yytextOrig != '|') { yytextOrig++; }
                        *yytextOrig = 0;
                        yylval.string = strdup(yytext);
    
                        yytextOrig++;
                        while (*yytextOrig) { yytextOrig++; }
                        yytextOrig--;
                        while (*yytextOrig) {
                            yyunput(*yytextOrig, yytext);
                            yytextOrig--;
                        }
                        yyunput(' ', yytext);
                        yyunput('|', yytext);
    
                        return WORD;
                    }

[^ \t\n][^ \t\n]*   {
						yylval.string = strdup(yytext);
						return WORD;
					}
%%
