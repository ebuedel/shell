#ifndef command_h
#define command_h

#ifdef __cplusplus
extern "C" {
#endif

void setInFile(const char * file);
void setOutFile(const char * file, int append);
void setErrFile(const char * file, int append);
void setBackground();

void wildcardExpand(char * prefix, char * suffix, int addSlash);
void insertArg(char * arg);
void insertCmdBuffer();
void insertSimpleCmd();
void executeCmd();
void printPrompt();
void yyerror(const char * msg);

#ifdef __cplusplus
}
#endif

#endif
