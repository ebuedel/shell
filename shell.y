%token <string> WORD

%token 	NOTOKEN GREAT NEWLINE GREATGREAT GREATGREATAMPERSAND GREATAMPERSAND LESS PIPE AMPERSAND 

%union { char * string; }

%{
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "shell.h"
void free(void *);
int yylex(void);

int yywrap() {
    extern FILE * yyin;
    if (yyin != stdin) {
        fclose(yyin);
        yyin = stdin;
        return 0;
    }
    return 1;
}
%}

%%

goal:	
	    commands
	    ;

commands:
	    command
	    | commands command
        |
        ;

command: simple_command
        ;

simple_command:
	    pipe_list iomodifier_list background NEWLINE { executeCmd(); }
        | NEWLINE { executeCmd(); }
        | error NEWLINE { yyerrok; }
        ;

pipe_list:
        pipe_list PIPE command_and_args
        | command_and_args
        ;

command_and_args:
        command_word argument_list { insertSimpleCmd(); }
	    ;

argument_list:
	    argument_list argument
	    |
	    ;

argument:
	    WORD { wildcardExpand("", $1, 0); insertCmdBuffer(); }
	    ;

command_word:
	    WORD { insertArg($1); }
        ;

iomodifier_list:
        iomodifier_list iomodifier
        | iomodifier
        |
        ;

iomodifier:
        GREAT WORD { setOutFile($2, 0); free($2); }
        | GREATGREAT WORD { setOutFile($2, 1); free($2); }
        | GREATAMPERSAND WORD { setOutFile($2, 0); setErrFile($2, 0); free($2); }
        | GREATGREATAMPERSAND WORD { setOutFile($2, 1); setErrFile($2, 1); free($2); }
        | LESS WORD  { setInFile($2); free($2); }
        ;

background:
        AMPERSAND { setBackground(); }
        |
        ;
%%
