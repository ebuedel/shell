#include <algorithm>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <regex>
#include <string>
#include <vector>

#include <dirent.h>
#include <fcntl.h>
#include <pwd.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

#include "shell.h"

#define biden execvp
#define D(...) __VA_ARGS__
//#define D(...)

namespace {
    bool debug = false;
    bool isExecuting = false;
    bool printErrorOnExecute = false;
    bool print = true;

    // stdin, stdout, and stderr redirection filenames
    std::string inFile;
    std::string outFile;
    std::string errFile;

    // path to shell program
    std::string shellPath;

    //std::string lastBgExecutedCmd;
    //int lastBgPid;
    //std::string lastCmdLine;

    // background and append
    bool background;
    bool append;

    // vectors to store commands
    std::vector<std::string> currSimpleCmd;
    std::vector<std::vector<std::string>> listOfSimpleCmds;
    std::vector<std::string> cmdBuffer;

    // vector to store background pids
    std::vector<int> backgroundPids;

    void ambiguousRedirect() { std::cout << "Ambiguous output redirect\n"; }

    void handleStuff(int signal) {
        if (signal == SIGINT) {
            std::cout << "\n";
            if (!isExecuting) { printPrompt(); }
        }

        int currpid = wait3(NULL, 0, NULL);
        while (waitpid(-1, NULL, WNOHANG) > 0)
            ;
        auto pid = std::find(backgroundPids.begin(), backgroundPids.end(), currpid);
        if (pid != backgroundPids.end()) {
            backgroundPids.erase(pid);
            std::cout << "[" << currpid << "] exited.\n";
            printPrompt();
        }
    }

    void setHandlers() {
        struct sigaction sa;
        sa.sa_handler = handleStuff;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = SA_RESTART;

        if (sigaction(SIGINT, &sa, NULL) < 0 || sigaction(SIGCHLD, &sa, NULL) < 0) {
            perror("sigaction: SIGINT or SIGCHLD");
            exit(1);
        }
    }

    void printCmdTable() {
        std::cout << "\n\n"
                  << "\t\tCOMMAND TABLE\n"
                  << "\n"
                  << "#   Simple Commands\n"
                  << "--- -----------------------------------------------------\n";

        int count = 0;
        for (auto simpleCmd : listOfSimpleCmds) {
            std::cout << std::setw(3) << std::left << ++count << " ";
            for (auto arg : simpleCmd) {
                std::cout << "\"" << arg << "\" \t";
            }
            std::cout << "\n";
        }

        std::cout << "\n\n"
                  << "Input        Output       Error        Background\n"
                  << "------------ ------------ ------------ ------------\n"
                  << std::setw(13) << std::left
                  << (inFile.empty() ? "default" : inFile) << std::setw(13)
                  << std::left << (outFile.empty() ? "default" : outFile)
                  << std::setw(13) << std::left
                  << (errFile.empty() ? "default" : errFile) << std::setw(13)
                  << std::left << (background ? "yes" : "no") << "\n" << std::endl;
    }

    void clearStdFiles() { inFile = outFile = errFile = ""; }

    bool checkForBuiltins() {
        if (listOfSimpleCmds[0][0] == "exit") { std::exit(0); }

        if (listOfSimpleCmds[0][0] == "setenv") {
            setenv(listOfSimpleCmds[0][1].c_str(), listOfSimpleCmds[0][2].c_str(), 1);
            return true;
        }
        if (listOfSimpleCmds[0][0] == "unsetenv") {
            unsetenv(listOfSimpleCmds[0][1].c_str());
            return true;
        }
        if (listOfSimpleCmds[0][0] == "cd") {
            if (listOfSimpleCmds[0].size() == 1) { return !chdir(getenv("HOME")); }
            return !chdir(listOfSimpleCmds[0][1].c_str());
        }
        if (listOfSimpleCmds[0][0] == "jobs") {
            for (auto pid : backgroundPids) {
                std::cout << "Background process: " << pid << "\n";
            }
            return true;
        }
        return false;
    }

    std::string removeBackslash(std::string arg) {
        // replace all instances of backslash except for two in a row
        std::regex regex("[^\\\\]{1}\\\\");
        std::string newArg = arg;
        std::vector<int> matchLocs;
        int offset = -1;
        for (auto iter = std::sregex_iterator(arg.begin(), arg.end(), regex); iter != std::sregex_iterator(); iter++) {
            newArg = newArg.replace(iter->position() - offset, 1, "");
            offset++;
        }

        // replace instance of backslash at beginning of arg
        std::regex regex2("^\\\\");
        if (std::regex_search(newArg, regex2)) { newArg = newArg.replace(0, 1, ""); }
        return (strdup(newArg.c_str()));
    }

    std::string expandTilde(std::string arg) {
        if (arg[0] == '~') {
            // ~ -> homedir
            if (arg.length() == 1)
                return getenv("HOME");

            // ~/ -> homedir/
            if (arg[1] == ' ' || arg[1] == '/')
                return arg.replace(0, 1, getenv("HOME"));

            // else parse the specified user's directory
            int i;
			for (i = 0; i != (int)arg.length() && arg[i] != '/'; i++)
                ;
            std::string nameToExpand;
            nameToExpand = arg.substr(1, i - 1);
            struct passwd * pass = getpwnam(strdup(nameToExpand.c_str()));

            // make sure that the user exists
            if (pass)
                arg = arg.replace(0, i, pass->pw_dir);
        }
        return arg;
    }

    void insertBufferArgs() {
        //sort the wildcard args
        std::sort(cmdBuffer.begin(), cmdBuffer.end());

        //insert the args in sorted order
        for (auto newArg : cmdBuffer) {
            currSimpleCmd.push_back(removeBackslash(strdup(newArg.c_str())));
        }

        //erase all elements in the current command buffer
        cmdBuffer.clear();
    }
}

extern "C" {

void printPrompt() {
    // only print prompt if stdin is a terminal
    if (isatty(STDIN_FILENO)) {
        std::cout << "> " << std::flush;
    }
}

void setInFile(const char * file) {
    if (!inFile.empty()) {
        ambiguousRedirect();
        return;
    }
    inFile = file;
}

void setOutFile(const char * file, int append) {
    if (!outFile.empty()) {
        ambiguousRedirect();
        return;
    }
    outFile = file;
    ::append = append;
}

void setErrFile(const char * file, int append) {
    if (!errFile.empty()) {
        ambiguousRedirect();
        return;
    }
    errFile = file;
    ::append = append;
}

void setBackground() { background = true; }

char * environExpand(char * arg) {
    std::string argStr = arg;
	free(arg);

    std::regex regex1("[$][{][$][}]");
    argStr = std::regex_replace(argStr, regex1, std::to_string(getpid()));

    std::regex regex2("[$][{][?][}]");
    argStr = std::regex_replace(argStr, regex2, "n/a" /*lastBgExecutedCmd*/);

    std::regex regex3("[$][{][!][}]");
    argStr = std::regex_replace(argStr, regex3, "n/a" /*std::to_string(lastBgPid)*/);

    std::regex regex4("[$][{]_[}]");
    argStr = std::regex_replace(argStr, regex4, "n/a" /*lastCmdLine*/);

    std::regex regex5("[$][{]SHELL[}]");
    argStr = std::regex_replace(argStr, regex5, shellPath);

    std::regex regex6("[$][{][^}][^}]*[}]");

    std::vector<std::string> matchStrings;
    std::vector<int> matchLocs;
    std::string newArgStr = argStr;

    int offset = 0;
    for (auto iter = std::sregex_iterator(argStr.begin(), argStr.end(), regex6); iter != std::sregex_iterator(); iter++) {
        std::string envName = iter->str();
        envName.erase(0, 2);
        envName.erase(envName.length() - 1, 1);
        newArgStr = newArgStr.replace(iter->position() - offset, iter->str().length(), getenv(envName.c_str()));
        offset += 3 + (envName.length() - strlen(getenv(envName.c_str())));
    }

    return strdup(newArgStr.c_str());
}

void insertArg(char * arg) {
    arg = environExpand(arg);
    std::string newArg = std::string(arg);
	free(arg);
    newArg = removeBackslash(newArg);
    newArg = expandTilde(newArg);
    currSimpleCmd.push_back(removeBackslash(newArg));
}

void insertCmdBuffer() {
    // sort the args
    std::sort(cmdBuffer.begin(), cmdBuffer.end());

    // insert each arg sorted
    for (auto arg : cmdBuffer) {
        insertArg(strdup(arg.c_str()));
    }

    // empty the buffer
    cmdBuffer.clear();
}

void wildcardExpand(char * prefix, char * suffix, int addSlash) {
    if (suffix[0] == 0) {
        // suffix is empty; insert the prefix
        cmdBuffer.push_back(strdup(prefix));
        return;
    }

    // else obtain the next part of the prefix
    char * substr = strchr(suffix, '/');
    char * component = new char[1024]();
    if (substr != NULL) {
        // copy up to the first '/'
        strncpy(component, suffix, substr - suffix + 1);
        suffix = substr + 1;
    } else {
        // last part of path; copy it all
        strcpy(component, suffix);
        suffix = suffix + strlen(suffix);
    }

    char * newPrefix = new char[1024]();
    if (!strchr(component, '*') && !strchr(component, '?')) {
        // component does not have wildcards
        sprintf(newPrefix, "%s%s", prefix, component);
        wildcardExpand(newPrefix, suffix, 0);
        return;
    }

    // component has wildcards
    bool dontMatchDot = false;
    std::string newArg;
    newArg += "^";
    char * a = component;
    while (*a) {
        if (*a == '?') {
            if (a == component) {
                newArg += "[^.]";
            } else {
                newArg += ".";
            }
        } else if (*a == '*') {
            if (a == component) {
                dontMatchDot = true;
            }
            newArg += ".*";
        } else if (*a == '.') {
            newArg += "\\.";
        } else {
            newArg += *a;
        }
        a++;
    }

    bool addSlashNext = false;
    if (newArg.back() == '/') {
        addSlashNext = true;
        newArg.erase(newArg.length() - 1, 1);
    }

    newArg += "$";

    std::regex regex(newArg);
    char * dirToOpen = new char[1024]();
    if (prefix[0] == 0) {
        strcpy(dirToOpen, ".");
    } else {
        strcpy(dirToOpen, prefix);
    }

    DIR * dir = opendir(dirToOpen);
    if (dir == NULL) {
        return;
    }

    struct dirent * entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_name[0] == '.' && dontMatchDot) {
            continue;
        }
        if (std::regex_match(entry->d_name, regex)) {
            sprintf(newPrefix, addSlash ? "%s/%s" : "%s%s", prefix, entry->d_name);
            wildcardExpand(newPrefix, suffix, addSlashNext);
        }
    }
    closedir(dir);
}

void insertSimpleCmd() {
    listOfSimpleCmds.push_back(currSimpleCmd);
    currSimpleCmd.clear();
}

void executeCmd() {
    // insert any wildcard arguments in buffer
    insertBufferArgs();

    if (!listOfSimpleCmds.size()) {
        printPrompt();
        return;
    }

    isExecuting = true;

    if (debug) { printCmdTable(); }

    if (printErrorOnExecute) {
        std::cout << currSimpleCmd.front() << ": No match.\n";
        printErrorOnExecute = false;
        clearStdFiles();
        currSimpleCmd.clear();
        listOfSimpleCmds.clear();
        printPrompt();
        background = false;
        isExecuting = false;
        return;
    }

    int in = dup(STDIN_FILENO);
    int out = dup(STDOUT_FILENO);
    int err = dup(STDERR_FILENO);

    int oldInFd, oldOutFd, oldErrFd;

    // save default in file
    if (inFile.empty()) {
        oldInFd = dup(STDIN_FILENO);
    } else {
        oldInFd = open(inFile.c_str(), O_RDONLY);
        dup2(oldInFd, STDIN_FILENO);
    }

    // save default out file
    if (outFile.empty()) {
        oldOutFd = dup(STDOUT_FILENO);
    } else {
        oldOutFd = open(outFile.c_str(), O_CREAT | O_WRONLY | (append ? O_APPEND : O_TRUNC), 0600);
        dup2(oldOutFd, STDOUT_FILENO);
    }

    // save default err file
    if (errFile.empty()) {
        oldErrFd = dup(STDERR_FILENO);
    } else {
        oldErrFd = open(errFile.c_str(), O_CREAT | O_WRONLY | (append ? O_APPEND : O_TRUNC), 0600);
        dup2(oldErrFd, STDERR_FILENO);
    }

    if (checkForBuiltins()) {
        clearStdFiles();
        currSimpleCmd.clear();
        listOfSimpleCmds.clear();
        printPrompt();
        background = false;

        close(oldInFd);
        close(oldOutFd);
        close(oldErrFd);

        dup2(in, STDIN_FILENO);
        dup2(out, STDOUT_FILENO);
        dup2(err, STDERR_FILENO);

        close(in);
        close(out);
        close(err);
        return;
    }

    int fdpipe[2];
    int pid;
    int nextInputFd = 0;
    auto remainingCmds = listOfSimpleCmds.size();

    for (auto simpleCmd : listOfSimpleCmds) {
        std::vector<char *> cmds;
        std::transform(simpleCmd.begin(), simpleCmd.end(), std::back_inserter(cmds), [](const std::string & s) { return (char*)s.c_str(); });
        cmds.push_back(nullptr);

        if (pipe(fdpipe) == -1) {
            perror("pipe");
            exit(1);
        }

        //make sure stdin is correct
        if (remainingCmds != listOfSimpleCmds.size()) {
            dup2(nextInputFd, STDIN_FILENO);
        }

        //make sure stdout is correct
        if (remainingCmds > 1) {
            dup2(fdpipe[1], STDOUT_FILENO);
        } else {
            dup2(oldOutFd, STDOUT_FILENO);
        }

        pid = fork();
        if (pid == -1) {
            perror("fork");
            exit(1);
        }
        if (pid == 0) {
            biden(cmds[0], cmds.data());
            perror(cmds[0]);
            exit(1);
        }

        nextInputFd = dup(fdpipe[0]);
        close(fdpipe[0]);
        close(fdpipe[1]);

        remainingCmds--;
    }
    if (!background) {
        waitpid(pid, 0, 0);
    } else {
        //lastBgPid = pid;
        backgroundPids.push_back(pid);
    }

    close(oldInFd);
    close(oldOutFd);
    close(oldErrFd);

    dup2(in, STDIN_FILENO);
    dup2(out, STDOUT_FILENO);
    dup2(err, STDERR_FILENO);

    close(in);
    close(out);
    close(err);

    clearStdFiles();
    currSimpleCmd.clear();
    listOfSimpleCmds.clear();
    if (print) { printPrompt(); }

    background = false;
    isExecuting = false;
}

void yyerror(const char * s) { std::cerr << s << std::flush; }

int yyparse(void);
}

int main(int argc, char ** argv) {
    if (argc > 1) {
        // this is being run by a subshell; do not print a prompt
        print = false;
        extern FILE * yyin;

        // extract arguments for subshell to run
        yyin = fmemopen(argv[1], strlen(argv[1]), "r");

        // execute the arguments and return
        yyparse();
        return 0;
    }
    print = true;
    shellPath = argv[0];
    setHandlers();
    if (print) { printPrompt(); }

    // begin parsing input
    yyparse();
    return 0;
}
