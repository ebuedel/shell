CC=clang
CFLAGS=-O2
CXXFLAGS=-std=c++14 -Wall -Wextra -Werror -g -O1
LEX=lex
YACC=yacc

all: shell

lex.yy.o: shell.l 
	$(LEX) shell.l
	$(CC) $(CFLAGS) -c lex.yy.c

y.tab.o: shell.y
	$(YACC) -d shell.y
	$(CC) $(CFLAGS) -c y.tab.c

shell.o: shell.cpp
	$(CC) $(CXXFLAGS) -c shell.cpp

shell: y.tab.o lex.yy.o shell.o tty-raw-mode.o read-line.o
	$(CC) -o shell lex.yy.o y.tab.o shell.o tty-raw-mode.o read-line.o -lfl -lstdc++

tty-raw-mode.o: tty-raw-mode.c
	gcc -c tty-raw-mode.c

read-line.o: read-line.cpp
	$(CC) $(CXXFLAGS) -c read-line.cpp

keyboard-example: keyboard-example.c tty-raw-mode.o
	gcc -o keyboard-example keyboard-example.c tty-raw-mode.o

read-line-example: read-line-example.c tty-raw-mode.o read-line.o
	gcc -o read-line-example read-line-example.c tty-raw-mode.o read-line.o

clean:
	rm -f lex.yy.c y.tab.c y.tab.h shell *.o
